 <!-- SPDX-License-Identifier: CC-BY-NC-SA-4.0 -->
 <!-- © 2023-2024 Adam Beer -->
# Packages
The [OpenWrt Firmware Selector website](https://firmware-selector.openwrt.org/?target=ramips%2Fmt7621&id=netgear_r6350) preselected these packages to install:
```
-uboot-envtools
base-files
busybox
ca-bundle
dnsmasq
dropbear
firewall4
fstools
kmod-gpio-button-hotplug
kmod-leds-gpio
kmod-mt7603
kmod-mt7615-firmware
kmod-nft-offload
kmod-usb-ledtrig-usbport
kmod-usb3
libc
libgcc
libustream-mbedtls
logd
luci
mtd
netifd
nftables
odhcp6c
odhcpd-ipv6only
opkg
ppp
ppp-mod-pppoe
procd
procd-seccomp
procd-ujail
uboot-envtools
uci
uclient-fetch
urandom-seed
urngd
wpad-basic-mbedtls
```
(This is true of firmware `23.05.0` through `23.05.3`.)

I then added these:
```
avahi-dbus-daemon
avahi-utils
bash
byobu
byobu-utils
dnscrypt-proxy2
luci-app-adblock
luci-app-attendedsysupgrade
nano-full
openssh-sftp-server
tcpdump
-wpad-basic-mbedtls
wpad-mbedtls
```

## Reasoning
The following packages are not necessarily self-explanatory:

- `openssh-sftp-server`: Allows `scp`-ing files to the device without requiring the `-O` flag. Also allows mounting a directory on a remote host using SSHFS, which in turn allows for using `chezmoi` remotely.
- `tcpdump`: Allows `adblock` to log and report DNS queries. Also allows manual logging of all traffic for troubleshooting purposes.
- `wpad-basic-mbedtls` -> `wpad-mbedtls`: Contains Wi-Fi Authenticator (server) and Supplicant (client). Dunno what the full one has that the basic doesn't but it's only ~250KiB bigger so 🤷. `mbedtls` is the TLS library; alternatives are `wolfssl` and `openssl`.
- `avahi-*`: Allows for printers to advertize themselves across subnets (see Hardware page).

## Chezmoi
My [chezmoi](https://chezmoi.io) files are synced with git. Problem: there's no chezmoi package for OpenWrt. Maybe I'll make one eventually. The x86 binary is 30MiB though.

<details><summary>Git dependency analysis</summary>

```
git
  libopenssl3 (~libmbedtls12)
    libatomic1
  zlib
```
```
libustream-mbedtls20201210 (<>libustream-openssl20201210)
  libmbedtls12
wpad-mbedtls (<>wpad-openssl)
  libmbedtls12
```
```
libustream-openssl20201210
  [...]
```
```
wpad-openssl
  libopenssl-legacy
    libopenssl-conf
  [...]
```

Conclusion: to add git, add `git libustream-openssl20201210 wpad-openssl`, and remove `libustream-mbedtls20201210 libmbedtls12 wpad-mbedtls`.</details>
