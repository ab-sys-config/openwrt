 <!-- SPDX-License-Identifier: CC-BY-NC-SA-4.0 -->
 <!-- © 2023-2024 Adam Beer -->
# Hardware
## Router
[Netgear R6350](https://openwrt.org/toh/netgear/r6350)

## Printer
[Epson WorkForce 845](https://epson.ca/For-Work/Printers/Inkjet/Epson-WorkForce-845-All-in-One-Printer/p/C11CB92201)

### Wi-Fi Compatibility
- Does not support WPA3
- Does not support *802.11w Management Frame Protection* => must be disabled, not even optional!

### Advertizing
- Install `avahi-utils` `avahi-dbus-daemon` packages on router.
- In `/etc/avahi/avahi-daemon.conf`, set `[reflector]` => `enable-reflector=yes`.
  - **WARNING:** This pierces firewall zones. To mitigate, set `[server]` => `allow-interfaces=br-lan,phy0-ap2`, where `br-lan` is the interface the client devices connect to, and `phy0-ap2`  is the interface the printer connects to. This is still suboptimal since it's bidirectional, but it's the best solution I've found.
- In DNSMASQ, create a CNAME entry for `<printer>.local` pointing to `<printer>.home.arpa`. I've experimented with other DNSMASQ settings but there doesn't seem to be any other way of fixing this.

## Small iPad
Joanna's small iPad is an [iPad Mini 2](https://en.wikipedia.org/wiki/IPad_Mini_2).
- Does not support WPA3
- Does not support *802.11w Management Frame Protection* => may be "optional."
- Does not automatically connect to hidden networks. 😬
