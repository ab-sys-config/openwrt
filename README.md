 <!-- SPDX-License-Identifier: CC-BY-NC-SA-4.0 -->
 <!-- © 2023-2024 Adam Beer -->
# openwrt
Various files documenting my home OpenWrt configuration

## License
I will endeavour to include an [SPDX short identifier](https://spdx.github.io/spdx-spec/v2.3/using-SPDX-short-identifiers-in-source-files/) at the start of each file in this repository; the lack thereof shall be considered a bug. In general, though, code in this repo is released under `AGPL-3.0-or-later`, and documentation under `CC-BY-NC-SA-4.0`.
