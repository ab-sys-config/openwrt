 <!-- SPDX-License-Identifier: CC-BY-NC-SA-4.0 -->
 <!-- © 2023 Adam Beer -->

# Backup

Files/Dirs added to backup:

```
/etc/dnscrypt-proxy2/
```
